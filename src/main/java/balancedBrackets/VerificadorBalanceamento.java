package balancedBrackets;

import java.lang.IllegalArgumentException;
import java.util.HashMap;
import java.util.Stack;


public class VerificadorBalanceamento {

    Stack<Character> brackets = new Stack<Character>();
    HashMap<Character, Character> conjuntoChavesBrackets = new HashMap<Character, Character>() {{
        put('{', '}');
        put('(', ')');
        put('[', ']');
        put(']', Character.MIN_VALUE);
        put(')', Character.MIN_VALUE);
        put('}', Character.MIN_VALUE);
    }};
    String texto;

    public VerificadorBalanceamento(String texto) {
        if (texto == null || texto.trim().isEmpty())
            throw new IllegalArgumentException("O Argumento precisa ser uma string valida!");
        this.texto = texto;
    }

    public Boolean balanceada() {
        for (char c : this.texto.toCharArray()) {
            char caractere = c;
            if (this.conjuntoChavesBrackets.containsKey(caractere))
                this.verificaBalanceamento(caractere);
        }
        return this.brackets.empty();
    }

    private void verificaBalanceamento(char caractere) {
        char bracketEsperado = this.conjuntoChavesBrackets.get(caractere);
        if (!this.brackets.isEmpty() && this.brackets.peek() == caractere)
            this.brackets.pop();
        else
            this.brackets.push(bracketEsperado);
    }

}
